<?php

/*
This filter aims to automatically generate the reference number and the lawyer's fullname before it is stored in the database
*/

function modify_data_from_request_form($post){
  if ($post) {

    // Selection of the research team
    global $wpdb;
  	$sql = $wpdb->prepare( "SELECT * FROM `countries_by_team` WHERE `country` = %s", $post['country_of_research']);
  	$research_team = $wpdb->get_row( $sql, ARRAY_A );
  	$post['research_team'] = ($research_team['team'] !== NULL) ? $research_team['team'] : 'global'; // If the country is not in the database, the research team is the global one, else it is the research team specified in the database

    // Variables used to generate the reference number.
    $current_year = date('Y');
    $team_prefix = strtoupper(substr($post['research_team'], 0, 3)); // Generate the prefix. For instance AFG or AFR

    // Get info about the last request in order to generate the right reference number
    $previous_cases_ids = Participants_Db::get_record_id_by_term("research_team",$post['research_team'], false); // Get the IDs of all past requests for the same team than the new request
    $last_case_id = end($previous_cases_ids); // Get the ID of the most recent request
    $last_case_infos = Participants_Db::get_participant($last_case_id); // Get info about the most recent case
	$ref_prefix = preg_replace("/[0-9-]/","",$last_case_infos['reference_number']); // Get the team prefix of the most recent request. For instance AFG in AFG2017-02

	  while($ref_prefix != $team_prefix) { // Check whether the prefix of the reference number is the same. This is a fix for specific cases with a reference number different from the research team.
		  $last_case_id = prev($previous_cases_ids);
		  $last_case_infos = Participants_Db::get_participant($last_case_id);
		$ref_prefix = preg_replace("/[0-9-]/","",$last_case_infos['reference_number']);
	  }

    $ref_num = explode("-",$last_case_infos['reference_number']); // Get the number of the most recent request. For instance, 02 in AFG2017-02.
    $ref_year = preg_replace("/[A-Z]/","",$ref_num[0]); // Get the year of the most recent request. For instance, 2017 in AFG2017-02

    if ($ref_year == $current_year) {
      $new_number = $ref_num[1] + 1; // If the most recent request has been made on the same year, then number of the new request is old one + 1
    }
    else {
      $new_number = 1; // Else, it is 1
    }

    // Generation of the reference number and the lawyer fullname.
    $post['reference_number'] = $team_prefix . $current_year . '-' . sprintf("%02d", $new_number); // Generate the reference number - sprintf to add a 0 before a single ciffer
    $ref_already_exists = Participants_Db::get_record_id_by_term("reference_number",$post['reference_number'], true); // Check in the database whether a record with the same reference number already exists.

    while (!empty($ref_already_exists)) { // While a record with the same reference number already exists, increment the reference number and check in the database.
      $new_number++;
      $post['reference_number'] = $team_prefix . $current_year . '-' . sprintf("%02d", $new_number); // Generate the reference number - sprintf to add a 0 before a single ciffer
      $ref_already_exists = Participants_Db::get_record_id_by_term("reference_number",$post['reference_number'], true);
    }

    $post['lawyerand39s_fullname'] = ucfirst($post['first_name']) . ' ' . strtoupper($post['last_name']); // Generate the lawyer fullname

    return $post;
  }
}

add_filter('pdb-before_submit_signup', 'modify_data_from_request_form');
add_filter('pdb-before_submit_add', 'modify_data_from_request_form');
