<?php
/*
After a research request has been submitted and stored in the database an email is sent to the research team's board on Trello.
*/

// Research requests form - send an email to Trello
function send_email_to_trello($post){
	if ($post) {

		// Selection of the research team
    global $wpdb;
	$data = array();
  	$sql = $wpdb->prepare( "SELECT * FROM `countries_by_team` WHERE `country` = %s", $post['country_of_research']);
  	$research_team = $wpdb->get_row( $sql, ARRAY_A );
  	$post['research_team'] = ($research_team['team'] !== NULL) ? $research_team['team'] : 'global'; // If the country is not in the database, the research team is the global one, else it is the research team specified in the database
	Participants_Db::write_participant(array('research_team' => $post['research_team']), $post['id']);

		// Change here the email adresses corresponding to trello boards
		$email_adresses = array('mena' => '*****',
                        'cis' => '*****',
                        'afghanistan' => '*****',
                        'africa' => '*****',
						            'global' => '*****');

		// Email of the UK consultant. Used to send an email to the consultant every time a request comes from the UK.
		$email_uk_consultant = '*****';

		// Trello's pseudos of the coordinators. Used to tag them when a requests is sent to their team. Do not forget to append a '@' before the pseudo.
		$trello_pseudos = array('mena' => '*** *** ***',
														'cis' => '*** *** ***',
														'afghanistan' => '*** *** ***',
														'africa' => '*** *** ***',
														'global' => '*** *** ***');

		//Change here the from email adress and the subject of the message (title of the Trello card)
		$from = '****';
		$subject = '***';

		// Change here the body of the email (description on the Trello card)
		$template_email_trello = '*****';


// Also send an email to the UK coordinators if the country of claim is UK
if ($post['country_of_asylum_claim'] == 'United Kingdom') {
	$email_to = $email_adresses[$post['research_team']] . ', ' . $email_uk_consultant;
}
else {
	$email_to = $email_adresses[$post['research_team']];
}

if (array_key_exists($post['research_team'], $email_adresses)) {
  $config = array(
 'to' => $email_to,
 'from' => $from,
 'subject' => $subject,
 'template' => $template_email_trello,
);
}
		else { // In case there is a error...
			$config = array(
 			'to' => '****',
 			'from' => $from,
 			'subject' => 'Oops - Error while sending the email to Trello',
 			'template' => 'An error occured after someone submitted a request.

			The request [reference_number] should be in the database but no Trello card has been created.'
		);
		}

PDb_Template_Email::send($config, $post);

}
}

add_filter('pdb-after_submit_signup', 'send_email_to_trello');
