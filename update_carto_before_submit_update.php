<?php

function update_carto($post) {

	$old_data = Participants_Db::get_participant($post['id']); // Get data currently in the database about the updated case.
  // If there is a change of status or of sharing policy
  if (($old_data['status'] != $post['status'] || $old_data['to_be_shared'] != $post['to_be_shared'])) {

    $dataset = 'number_of_cases_by_country_bubbles'; //Name of the dataset in Carto
    $carto_API_key = "****";
    $api_url = '****';

    // Initialise some useful variables, no need to change these normally.
    $country = $post['country_of_research'];
    $country_sql = str_replace("'", "''", $country);
    $country_url = str_replace("'", "%27", $country);

    // If the case is going to be shared on the platform
    if ($post['to_be_shared'] == 'yes' && ($post['status'] == 'completed' || $post['status'] == 'edited')) {

      // Prepare sql request to call CARTO API to check whether there is already a row for this country
      $carto_sql_call_query = "SELECT count(*) FROM " . $dataset . " WHERE country = '" . $country_sql . "'";

      $carto_api_query = array(
        'q' => $carto_sql_call_query,
        'api_key' => $carto_API_key
      );

      // Call CARTO API to count the number of row
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$api_url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $carto_api_query);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $count_result_json = curl_exec($ch);
      curl_close($ch);

      $count_result = json_decode($count_result_json, true);
	  //$count_result = $count_result_object->rows['count'];

      if ($count_result['rows'][0]['count'] < 1 ) {   // If there is no row corresponding to the current country in Carto dataset we create the row

        $number_of_cases = 1; //Initialise the number of cases at 1

        // Set up the link to the database
        $link_to_db = str_replace("'", "''", "<a href='https://resources.asylos.eu/available-research/?search_field=country_of_research&target_instance=2&value=" . $country_url . "' target='_blank'>See list of reports on " . $country . "</a>");

        // Prepare sql query to insert a new row for this country
        $carto_sql_query = "INSERT INTO " . $dataset . " (country, link_to_database, number_of_cases) VALUES ('" . $country_sql . "', '" . $link_to_db . "', " . $number_of_cases . ")";

        // Prepare another sql query to geocode the new row
        $carto_geocode_query = "UPDATE " . $dataset . " SET the_geom = cdb_geocode_street_point(country) WHERE country = '" . $country_sql . "'";
      }
      else { // If there is already a row to the current country in Carto dataset, we update the row with the new data

        global $wpdb; // Initialise access to Wordpress database
        $number_of_cases = $wpdb->query($wpdb->prepare( // Get the number of shared cases on this specific country
          "
          SELECT *
          FROM `fmk_3_participants_database`
          WHERE (`status` = %s
          OR `status` = %s)
          AND `to_be_shared` = %s
          AND `country_of_research` = %s
          AND NOT `id` = %d
          ",
          'edited',
          'completed',
          'yes',
          $country,
          $post['id'] // This is to prevent the current case to be counted (e.g. when changing status from completed to edited)
        ));

        $number_of_cases++; //Increment the number of case (because we are currenty adding one case)

        // Prepare sql query to update the row corresponding to this country
        $carto_sql_query = "UPDATE " . $dataset . " SET number_of_cases = " . $number_of_cases . " WHERE country = '" . $country_sql . "'";
      }
    }
    // If the case was shared but is not going to be anymore
    elseif ($old_data['to_be_shared'] == 'yes' && ($old_data['status'] == 'completed' || $old_data['status'] == 'edited')) {
      global $wpdb; // Initialise access to Wordpress database
      $number_of_cases = $wpdb->query($wpdb->prepare( // Get the number of shared cases on this specific country
        "
        SELECT *
        FROM `fmk_3_participants_database`
        WHERE (`status` = %s
        OR `status` = %s)
        AND `to_be_shared` = %s
        AND `country_of_research` = %s
        AND NOT `id` = %d
        ",
        'edited',
        'completed',
        'yes',
        $country,
        $post['id'] // This is to prevent the current case to be counted
		));

        // Prepare sql query to update the row corresponding to this country
        $carto_sql_query = "UPDATE " . $dataset . " SET number_of_cases = " . $number_of_cases . " WHERE country = '" . $country_sql . "'";
    }


    // Prepare the API query
    $carto_api_query = array(
      'q' => $carto_sql_query,
      'api_key' => $carto_API_key
    );

    // Send the query to Carto API
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$api_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $carto_api_query);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);

    if (isset($carto_geocode_query)) { // If there is a need to geocode (new row for new country)

      // Prepare a new API query
      $carto_api_query = array(
        'q' => $carto_geocode_query,
        'api_key' => $carto_API_key
      );

      // Send the new query to Carto API (geocode the new row)
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$api_url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $carto_api_query);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec($ch);
      curl_close($ch);
    }
  }
  return $post;
}

add_filter( 'pdb-before_submit_update', 'update_carto' );
